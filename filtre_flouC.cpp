// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
using namespace std;

bool* genMask(int size)
{
  bool* m = new bool[size*size];

  int size2 = (size/2);
  for(int dx = -size2; dx < size2;dx++)
  {
    for(int dy = -size2; dy < size2;dy++)
    {
      int mx = size2+dx;  int my = size2+dy;
      int d = pow(abs(dx)+1,2)+pow(abs(dy)+1,2)-1;
      m[(mx*size)+my] = d <= (size2*size2);
    }
  }
  return m;
}

int moyenne(OCTET* Img, int imgSize,int x, int y,bool* mask, int maskSize)
{
  int size2 = maskSize/2;
  int res = 0; int count = 0;
  for(int dx = -size2; dx < size2;dx++)
  {
    for(int dy = -size2; dy < size2;dy++)
    {
      int mx = size2+dx; int my = size2+dy;

      if(mask[(mx*maskSize)+my] == false){continue;}

      int xx = x + dx; int yy = y + dy;

      if(xx < 0 || xx >= imgSize || yy < 0 || yy >= imgSize){continue;}
    
      res += Img[(yy*imgSize)+xx];
      count++;
    }
  }

  return res / count;
}

void flouteur(char* cNomImgLue, char* cNomImgEcrite, int radius)
{
  int nH, nW, nTaille;
  OCTET *ImgIn,*ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgOut, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

  bool* mask = genMask(radius);

  for(int i = 0; i < nW;i++)
  {
    for(int j = 0; j < nH;j++)
    {
      if(i==0||j==0||i==nW-1||j==nH-1)
      {
        ImgOut[j+(i*nH)] = ImgIn[j+(i*nH)];
      }
      else
      {
        ImgOut[j+(i*nH)] = moyenne(ImgIn,nH,j,i,mask,radius);
      }
    }
  }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int radius;

  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm  ImgOut.pgm radius\n"); 
       exit (1) ;
     }
  printf("Lecture des args\n");

   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
  sscanf (argv[3],"%d",&radius);

  printf("radius : %d",radius);

  flouteur(cNomImgLue, cNomImgEcrite,radius);

   return 1;
}
