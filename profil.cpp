// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
using namespace std;
int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomHistoEcrit[250], colorline;
  int nH, nW, nTaille,id;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm   HistoOut.dat   C/L (Col/Line)  id  \n"); 
       exit (1) ;
     }
  printf("Lecture des args\n");

   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomHistoEcrit);

  sscanf (argv[3],"%c",&colorline);
  sscanf (argv[4],"%i",&id);

  printf("Fait !\n");

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

  int hl = colorline == 'L' ? nW : nH;
  int histo[hl];

  for(int i = 0; i < hl;i++)
  {
    histo[i] = colorline == 'L' ? ImgIn[(id*nW)+i]:ImgIn[(i*nH)+id];
  }
/*
  int histo[256];
  for(int i = 0; i < 256;i++){histo[i]=0;}

  if(colorline == 'L'){
   for (int j=0; j < nW; j++)
     {
       int v =ImgIn[id*nW+j];
      histo[v]++;
     }
  }
  else if(colorline=='C')
  {
    for (int j=0; j < nH; j++)
     {
       int v =ImgIn[j*nH+id];
      histo[v]++;
     }
  }
  */

  ofstream flux(cNomHistoEcrit);
   for(int i =0;i < 256;i++)
   {
     cout << i <<" " << histo[i] << endl;
     flux << i << " " << histo[i] << endl;
   }
   free(ImgIn); 

   return 1;
}
